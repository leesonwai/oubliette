/* oubliette.c
 *
 * Copyright 2020 The Oubliette authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "config.h"
#include "dungeon.h"
#include "monster.h"

#define BASE_HP           8
#define BASE_STR          4
#define CEIL(f)           (int) (f) + 1
#define HP_ROLLS          3
#define LEN(v)            sizeof ((v)) / sizeof ((v)[0])
#define LOAD_SECS         3
#define MAXBUF            64
#define MAXNAME           32
#define NO_ENC_CHANCE     0.4f
#define STR_ROLLS         2
#define VOWEL(c)          tolower ((c)) == 'a' || \
                          tolower ((c)) == 'e' || \
                          tolower ((c)) == 'i' || \
                          tolower ((c)) == 'o' || \
                          tolower ((c)) == 'u'

struct player
{
  char         name[MAXNAME];
  int          hp;
  int          str;
  struct node *pos;
};

static const char *prog;

static int
d6roll (int nrolls)
{
  int roll = 0;

  while (nrolls--)
    roll += rand () % 6;

  return roll;
}

static void
die (const char *fmt,
     ...)
{
  va_list ap;

  va_start (ap, fmt);
  vfprintf (stderr, fmt, ap);
  va_end (ap);

  exit (EXIT_FAILURE);
}

static void
pseudoload (int s)
{
  while (s--)
    {
      sleep (1);
      write (STDOUT_FILENO, ".", 1); /* Is this as horrible as it feels? */
    }
  putchar ('\n');
}

static int
pathmatch (char *path,
           int   id)
{
  static const char *paths[] = {"North", "East", "South", "West"};

  for (int i = 0; i < MAXPATHS; ++i)
    {
      if (!strcasecmp (path, paths[i]) && dungeon[id].paths[i])
        return i;
    }

  return -1;
}

static int
getnline (char *buf,
          int   lim)
{
  int c;
  int len = 0;

  while ((c = getchar ()) != '\n' && --lim)
    buf[len++] = c;
  buf[len] = '\0';

  return len;
}

static void
fight (struct player  *pc,
       struct monster *m)
{
  int pcroll;
  int monhp = m->hp; /* Otherwise, subsequent monsters will spawn dead. */
  int monroll;

  printf ("%s %s ambushes you!\n", VOWEL (m->type[0]) ? "An" : "A", m->type);

  while (pc->hp > 0 && monhp > 0)
    {
      pcroll = pc->str + d6roll (1);
      monroll = m->str + d6roll (1);

      pseudoload (LOAD_SECS);

      if (pcroll > monroll)
        {
          monhp -= pc->str;
          printf ("You hit the %s for %d!\n", m->type, pc->str);
        }
      else if (monroll > pcroll)
        {
          pc->hp -= m->str;
          printf ("The %s hits you for %d!\n", m->type, m->str);
        }
      else
        {
          printf ("Miss!\n");
        }
    }

  if (monhp <= 0)
    printf ("The %s lies dead at your mighty feet.\n", m->type);
  else if (pc->hp <= 0)
    printf ("Oh dear, you are dead!\n");
}

static struct monster *
randenc (void)
{
  size_t x = rand () % (LEN (monsters) + CEIL (LEN (monsters) * NO_ENC_CHANCE));

  if (x > LEN (monsters) - 1)
    return NULL;

  return &monsters[x];
}

static struct node *
getmove (struct player *pc)
{
  char buf[MAXBUF];
  int path;

  do
    {
      printf ("Go... ");
      getnline (buf, MAXBUF);
      path = pathmatch (buf, pc->pos->id);
    }
  while (path == -1);

  return pc->pos->paths[path];
}

static void
adventure (struct player *pc)
{
  struct monster *m;

  while (!pc->pos->st && pc->hp > 0)
    {
      printf ("%s\n", descr[pc->pos->id]);
      pc->pos = getmove (pc);
      if ((m = randenc ()))
        fight (pc, m);
      else
        printf ("You pass through the corridor unmolested.\n");
    }

  if (pc->hp > 0)
    printf ("%s\n", descr[pc->pos->id]);
}

static struct player *
pcnew (void)
{
  struct player *pc = malloc (sizeof (struct player));

  if (!pc)
    die ("%s: %s\n", prog, strerror (errno));

  printf ("Enter your name, adventurer... ");

  getnline (pc->name, MAXNAME);
  pc->hp = BASE_HP + d6roll (HP_ROLLS);
  pc->str = BASE_STR + d6roll (STR_ROLLS);
  pc->pos = &dungeon[0];

  printf ("You are %s (%d STR and %d HP)\n", pc->name, pc->str, pc->hp);

  return pc;
}

static void
help ()
{
  printf ("Usage: %s [OPTION]\n"
          "A simple text-based adventure.\n\n"
          "  -h\tPrint help\n"
          "  -v\tPrint program version\n", prog);
}

int
main (int    argc,
      char **argv)
{
  struct player *pc;

  errno = 0;
  prog = basename (*argv);
  srand (time (NULL));

  if (argc > 2)
    die ("Usage: %s [OPTION]\n", prog);

  switch (getopt (argc, argv, "hv"))
    {
    case -1:
      break;
    case 'h':
      help ();
      return EXIT_SUCCESS;
    case 'v':
      printf ("%s %s\n", prog, VERSION);
      return EXIT_SUCCESS;
    default:
      printf ("Try '%s -h' for more information\n", prog);
      return EXIT_FAILURE;
    }

  pc = pcnew ();
  adventure (pc);

  free (pc);

  return EXIT_SUCCESS;
}
