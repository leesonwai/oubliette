/* dungeon.h
 *
 * Copyright 2020 The Oubliette authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdio.h>

#define MAXPATHS           4

struct node
{
  int          id;
  struct node *paths[MAXPATHS];
  int          st;
};

/*
 * The node's possible paths are defined in the following order:
 *
 *  {&north_path, &east_path, &south_path, &west_path}
 *
 * Each path is simply the address of another node in the array. If there's
 * no exit in that particular position, it's NULL instead.
 */

struct node dungeon[] =
{
  {0, {&dungeon[1], NULL, NULL, NULL}, 0},
  {1, {&dungeon[2], NULL, &dungeon[0], NULL}, 0},
  {2, {&dungeon[3], &dungeon[4], &dungeon[1], NULL}, 0},
  {3, {&dungeon[2], NULL, &dungeon[2], NULL}, 0},
  {4, {&dungeon[1], NULL, NULL, &dungeon[2]}, 1}
};

const char *descr[] =
{
  "You're in room 0",
  "You're in room 1",
  "You're in room 2",
  "You're in room 3",
  "You're in room 4"
};
