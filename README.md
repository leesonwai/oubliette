# oubliette
### Gaming like it's 1960
![License: GPL3](https://img.shields.io/badge/license-GPL3-green?style=flat-square)

`oubliette` is a simple text-based adventure game developed as part of a group
project. It intends to approximate a real game, but prioritises simplicity and
speed of implementation over fun, features and common sense.

Both the dungeon "map" and variety of monsters you can encounter can be modified
by hacking on `dungeon.h` and `monsters.h`, respectively.

## Installing
`oubliette` is developed on Linux, but it should run on Windows using [MSYS2](https://www.msys2.org/).
To build the game conveniently, install [Meson](https://mesonbuild.com/) and run
the following commands:

```
meson builddir
cd builddir/
ninja
sudo ninja install
```
